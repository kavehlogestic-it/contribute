# AI Workbench

### Who are we?
A group of AI enthusiasts who'd like to share their own project, or use other projects as example for their own experiments.

### What is our goal?
Creating a platform to share AI knowledge through our projects.

### How to contribute?
- Read the documentation in this repository to understand common guidelines.
- Request access to the AI workbench repository
- Add a new repository for your project to the AI workbench subgroup.
- Time to contribute!

Alternatively, contact us to include a reference to a project that is already openly available. This can be done by creating an issue in this repo, or mailing to `info@digicampus.tech`.

### Licensing

In order to allow others to reproduce, learn from, modify and use the experiments presented in the workbench all content is appropriately licensed. Our licenses of choice are Creative Commons v4, Attribution-ShareAlike for documentation and assets. GPL v3 is our preferred license for code.

The content in this repository is licensed under Creative Commons v4, Attribution-ShareAlike. See the [LICENSE file](./LICENSE) or the [online summary](https://creativecommons.org/licenses/by-sa/4.0/) for details.


### About Digicampus

[Digicampus](https://www.dedigicampus.nl/) is a meeting place for public, commercial, scientific and civilian parties alike.
We discover, experience, design and learn together. 

