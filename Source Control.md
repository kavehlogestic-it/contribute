# Source Control
Version control for all projects is maintained using [git](https://www.atlassian.com/git).

## Branching workflow
- Use the `master` branch as the latest (production-ready) state of your project.
- Create a `feature/myNewFeature` branch to add new, or change existing functionality.
  - Merge this `feature` back to `master` only when it is finished for production.
  - After a successful merge, the `feature` branch can be completely removed from origin.
